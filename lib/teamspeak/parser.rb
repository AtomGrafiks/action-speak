# frozen_string_literal: true

module Teamspeak
  module Parser
    # The following characters need to be escaped if they are to be used
    SPECIAL_CHARS = [
      ['\\\\', '\\'],
      ['\\/', '/'],
      ['\\s', ' '],
      ['\\p', '|'],
      ['\\a', '\a'],
      ['\\b', '\b'],
      ['\\f', '\f'],
      ['\\n', '\n'],
      ['\\r', '\r'],
      ['\\t', '\t'],
      ['\\v', '\v']
    ].freeze

    # Array of commands that are expected to return as an array.
    RESPONSE_ARRAY = %w[
      bindinglist serverlist servergrouplist servergroupclientlist servergrouppermlist
      servergroupsbyclientid servergroupclientlist logview channellist
      channelfind channelgrouplist channelgroupclientlist channelgrouppermlist
      channelpermlist clientgetids clientlist clientfind clientdblist clientdbfind
      channelclientpermlist permissionlist permoverview privilegekeylist
      messagelist complainlist banlist ftlist custominfo permfind
      serversnapshotcreate
    ].freeze

    def parse_response(response_string)
      array = []
      errors = []
      response_string.split('|').each do |elem|
        hash = {}
        elem.split(' ').map do |data|
          value = data.split('=', 2)
          errors.push(decode(value[1])) && next if (errors?(response_string) && value[0] == 'msg' && value[1] != 'ok')
          next if %w[id error msg].include?(value[0])
          hash[value[0]] = parse_value(decode(value[1]))
        end
        array.push(hash)
      end
      # return array.first if array.length == 1
      [array, errors.reject(&:nil?)]
    end

    def parse_value(value)
      return value unless value.to_s.scan(/,/).any?

      value.split(',').map do |param|
        param.n? ? param.to_n : param
      end
    end

    def errors?(string)
      string.scan(%r/((^|\W)id=[^0][0-9]*)|(error)/i).flatten.reject(&:nil?).length >= 2
    end

    def format_params(params)
      params.map { |key, value| "#{key}=#{encode(value)}" }.join(' ')
    end

    def format_options(options)
      options.map { |opt| opt.dup.prepend('-') }.join(' ')
    end

    def decode(param)
      return nil unless param
      return param.to_n if param.n?

      SPECIAL_CHARS.each { |pair| param.to_s.gsub!(pair[0], pair[1]) }
      param
    end

    def encode(param)
      SPECIAL_CHARS.each { |pair| param.to_s.gsub!(pair[1], pair[0]) }
      param
    end
  end
end
