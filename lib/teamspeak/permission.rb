# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class Permission < ActionSpeak
    include Teamspeak::Exceptions::DynAccessor

    attr_reader   :id, :name, :desc

    @@cache_list = nil

    needed_permissions %i[b_virtualserver_servergroup_list]

    def initialize
      @connect_bypass = true
      super
    end

    def build(permission_id)
      @@cache_list ||= Teamspeak::ActionSpeak.new.command_and_disconnect('permissionlist')
      @@cache_list.select { |h| h['permid'] == permission_id }.first.each do |key, value|
        dyn_accessor(key.gsub(/^perm/, ''), value)
      end

      self
    end

    def self.list
      Teamspeak::ActionSpeak.new.command_and_disconnect('permissionlist').map do |e|
        find(e['permid'])
      end
    end

    def self.find(permission_id)
      new.build(permission_id)
    end

    def self.find_by_name(permission_name)
      perm = Teamspeak::ActionSpeak.new.command_and_disconnect('permidgetbyname', permsid: permission_name)
      find(perm['permid'])
    end
  end
end
