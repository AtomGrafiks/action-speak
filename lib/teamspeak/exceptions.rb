require_relative 'exceptions/extender.rb'

module Teamspeak
  module Exceptions
    class Teamspeak::OpenedSocketInActionSpeak < StandardError; end
    class Teamspeak::EmptyCommand < StandardError; end
    class Teamspeak::InvalidServer < StandardError; end
    class Teamspeak::DisconnectedClient < StandardError; end
    class Teamspeak::TeamspeakError < StandardError
      def initialize(errors)
        raise begin
          "teamspeak/#{errors.join('_and_').tr(' ', '_')}".acronym('ID').camelize.constantize
        rescue NameError
          Teamspeak.const_set(errors.join('_and_').tr(' ', '_').acronym('ID').camelize, Class.new(StandardError))
        end
      end
    end
  end
end
