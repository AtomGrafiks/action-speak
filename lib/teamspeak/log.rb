# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class Log < ActionSpeak

    LEVEL_ERROR   = 1
    LEVEL_WARNING = 2
    LEVEL_DEBUG   = 3
    LEVEL_INFO    = 4

    needed_permissions %i[
      b_serverinstance_log_view b_virtualserver_log_view
      b_serverinstance_log_add b_virtualserver_log_add
    ]

    def self.view(args = {})
      Teamspeak::ActionSpeak.new.command_and_disconnect('logview', args)
    end


    def self.create(hash) 
      check = %i[level msg]
      raise ArgumentError, "args don't include level or msg" unless hash.transform_keys!(&:to_sym).keys & check == check

      new.command_and_disconnect('logadd', hash.transform_keys! { |k| "log#{k}" })
    end
  end
end
