# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class SendMessage < ActionSpeak
    TARGET_CLIENT   = 1
    TARGET_CHANNEL  = 2
    TARGET_SERVER   = 3

    needed_permissions %i[
      i_client_private_textmessage_power i_client_needed_private_textmessage_power
      b_client_server_textmessage_send b_client_channel_textmessage_send
      b_serverinstance_textmessage_send
    ]

    %w[client channel server].each do |action|
      define_singleton_method(:"to_#{action}") do |action_id, message|
        args = { targetmode: const_get(:"TARGET_#{action.upcase}"), target: action_id, msg: message }

        new.command_and_disconnect('sendtextmessage', args)
      end
    end

    def self.to_all(message)
      new.command_and_disconnect('gm', {msg: message})
    end
  end
end
