require 'securerandom'

module Teamspeak
  class ActionSpeak
    include ::Teamspeak::Parser

    # access to raw socket
    attr_accessor :sock

    # global informations to connect to server
    attr_accessor :uri, :username

    # Connection is protected ? Default : true
    attr_accessor :flood_protection, :flood_limit, :flood_interval

    # Connect bypass ?
    attr_accessor :connect_bypass

    # permissions nested to perform command
    class << self
      attr_accessor :permissions
    end

    def initialize(uri = ENV['TEAMSPEAK_SERVERQUERY_URI'] || 'tcp://127.0.0.1:10011', username = (ENV['TEAMSPEAK_SERVERQUERY_USERNAME'] || 'serveradmin'), password = ENV['TEAMSPEAK_SERVERQUERY_PASSWORD'], virtual_server_id = 1)
      @uri = URI(uri)
      @username = username

      connect && login(password) && use(virtual_server_id) && client_rename unless @connect_bypass == true
    end

    def connect
      @sock = TCPSocket.new(@uri.host, @uri.port)

      # Check if the response is the same as a normal teamspeak 3 server.
      raise Teamspeak::InvalidServer, 'Server is not responding as a normal TeamSpeak 3 server.' if @sock.gets.strip != 'TS3'

      # Remove useless text from the buffer.
      @sock.gets
      @sock.set_encoding('UTF-8')
    end

    def disconnect
      @sock.puts('quit')
      @sock.close
    end

    def login(password)
      @sock.puts("login client_login_name=#{@username} client_login_password=#{password}")
      @sock.gets
    end

    def use(server_id = @virtual_server_id)
      @sock.puts("use #{server_id}")
      @sock.gets
    end

    def prefix
      'ruby_speak'
    end

    def raw_command(command, params = {}, options = %w[])
      raise Teamspeak::EmptyCommand if command == ''
      flood_control

      raise Teamspeak::OpenedSocketInActionSpeak if command == 'servernotifyregister'

      @sock.puts([command, format_params(params), format_options(options)].join(' '))

      response = ''
      loop do
        response += (begin
                       @sock.gets
                     rescue StandardError
                       break
                     end)
        break if response.index(' msg=')
      end

      response
    end

    # Sends command to the TeamSpeak 3 server and returns the response
    def command(command, params = {}, options = %w[])
      parsed_response, errors = parse_response(raw_command(command, params, options))

      raise Teamspeak::TeamspeakError.new(errors) if errors.any?

      Teamspeak::Parser::RESPONSE_ARRAY.include?(command) ? parsed_response : parsed_response.first
    rescue Errno::EPIPE
      connect && retry
    end

    def command_and_disconnect(command, params = {}, options = %w[])
      res = command(command, params, options)
      disconnect

      res
    end

    def flood_control
      return unless @flood_protection

      @flood_current += 1

      flood_time_reached = (Time.now - @flood_timer < @flood_interval)
      flood_limit_reached = (@flood_current == @flood_limit)

      sleep(@flood_interval) if flood_time_reached && flood_limit_reached

      return unless flood_limit_reached

      @flood_timer = Time.now
      @flood_current = 0
    end

    def instance_variables
      super - %i[@uri @username @sock]
    end

    def attr_readers
      instance_variables.map { |k| k.to_s.delete('@').to_sym }.reject do |ivar|
        respond_to?("#{ivar}=")
      end
    end

    def prepare_hash(hash = {})
      hash.reject! do |k, _|
        attr_readers.include?(k.to_sym)
      end
    end

    def update(hash = {})
      return if hash.empty?

      hash.each do |key, value|
        instance_variable_set(:"@#{key}", value)
      end

      save
    end

    def save
      hash = {}

      instance_variables.each do |var|
        hash[var.to_s.delete('@')] = instance_variable_get(var)
      end

      hash
    end

    def permissions
      self.class.permissions
    end

    def self.needed_permissions(array_of_permissions)
      @permisisons = (@permisisons || []) + array_of_permissions

      send(:permissions=, @permisisons.uniq)
    end

    protected

    def client_rename
      @sock.puts("clientupdate client_nickname=#{encode("#{prefix}_#{SecureRandom.hex(10)}")}")
      @sock.gets
    end
  end
end
