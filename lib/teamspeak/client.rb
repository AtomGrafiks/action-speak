# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class Client
    include Teamspeak::Exceptions::Extender
    include Teamspeak::Exceptions::DynAccessor

    attr_accessor :clid
    attr_reader   :cid, :cldbid

    KICK_CHANNEL_REASON_ID    = 4
    SERVER_CHANNEL_REASON_ID  = 5

    # needed_permissions %i[
    #   b_virtualserver_client_list i_channel_subscribe_power i_channel_needed_subscribe_power
    #   b_client_info_view b_virtualserver_client_search b_client_modify_description b_client_set_talk_power
    #   b_virtualserver_client_dblist b_virtualserver_client_dbinfo b_virtualserver_client_dbsearch
    #   b_client_modify_dbproperties b_client_modify_description b_client_set_talk_power
    #   b_client_delete_dbproperties
    #   i_client_move_power i_client_needed_move_power
    #   i_client_kick_from_server_power i_client_kick_from_channel_power i_client_needed_kick_from_server_power i_client_needed_kick_from_channel_power
    #   i_client_poke_power i_client_needed_poke_power
    #   b_virtualserver_client_permission_list
    #   i_group_modify_power i_group_needed_modify_power i_permission_modify_power
    #   i_group_modify_power i_group_needed_modify_power i_permission_modify_power
    # ]

    def build(database_id, extra = {})
      extra.merge(Teamspeak::ActionSpeak.new.command_and_disconnect('clientdbinfo', cldbid: database_id)).each do |key, value|
        dyn_accessor(key.gsub('client_', ''), value)
      end

      self
    end

    ##################################
    # DB REST METHODS
    ##################################
    def delete 
      Teamspeak::ActionSpeak.new.command_and_disconnect('clientdbdelete', cldbid: database_id)
      true
    end

    def update(hash = {})
      return if hash.empty?

      hash.each do |key, value|
        instance_variable_set(:"@#{key}", value)
      end

      save
    end

    def save
      Teamspeak::ActionSpeak.new.command_and_disconnect('clientdbedit', {cldbid: database_id, client_description: description})
    end
    
    ##################################
    # LISTS
    ##################################

    # opts : -uid -away -voice -times -groups -info -icon -country
    def self.list (hash = {}, opts = %w[])
      if hash.transform_keys!(&:to_sym)[:connected] == true
        opts.select!{ |key| %w[uid away voice times groups info icon country].include?(key) }
        Teamspeak::ActionSpeak.new.command_and_disconnect('clientlist', {}, opts).map do |cl|
          begin
            new.build(cl['client_database_id'], cl)
          rescue Teamspeak::InvalidClientID
            nil
          end
        end.reject(&:nil?)
      else
        hash.select!{ |key, _| %i[offset limit].include?(key) }
        Teamspeak::ActionSpeak.new.command_and_disconnect('clientdblist', hash.any? ? {start: hash[:offset], duration: hash[:limit]} : {}).map do |cldb|
          new.build(cldb['cldbid'])
        end
      end
    end


    ##################################
    # FINDERS 
    ##################################
    def self.find(client_id)
      client = new
      Teamspeak::ActionSpeak.new.command_and_disconnect('clientinfo', clid: client_id).each do |key, value|
        client.dyn_accessor(key.gsub('client_', ''), value)
      end
      client.clid = client_id

      client
    end

    def self.find_by_database_id(database_id)
      new.build(database_id)
    end

    def self.find_by_unique_identifier(uid)
      res = Teamspeak::ActionSpeak.new.command_and_disconnect('clientdbfind', {pattern: uid}, %w[uid]).map do |h|
        new.build(h['cldbid'])
      end
      res.length == 1 ? res.first : res
    end

    def self.find_by_nickname(nickname)
      database_id = Teamspeak::ActionSpeak.new.command_and_disconnect('clientdbfind', pattern: nickname)[0]['cldbid']
      new.build(database_id)
    end

    def self.database_id_from_uid(uid)
      Teamspeak::ActionSpeak.new.command_and_disconnect('clientgetdbidfromuid', cluid: uid)['cldbid']
    end

    def self.name_from_uid(uid)
      Teamspeak::ActionSpeak.new.command_and_disconnect('clientgetnamefromuid', cluid: uid)['name']
    end

    def self.name_from_database_id(database_id)
      Teamspeak::ActionSpeak.new.command_and_disconnect('clientgetnamefromdbid', cldbid: database_id)['name']
    end

    def self.clid_from_nickname(nickname)
      Teamspeak::ActionSpeak.new.command_and_disconnect('clientfind', pattern: nickname)[0]['clid']
    end

    def self.online_find_by_nickname(nickname)
      client = find_by_nickname(nickname)

      raise Teamspeak::DisconnectedClient unless client.online?
      client.clid = clid_from_nickname(nickname)
      client
    end

    ##################################
    # ONLINE
    ##################################
    def online?
      client_ids.any?
    end

    def client_ids
      Teamspeak::ActionSpeak.new.command_and_disconnect('clientgetids', cluid: unique_identifier).map { |client| client['clid'] }
    rescue e('Teamspeak::DatabaseEmptyResultSet')
      []
    end

    def move(client_id_or_all, channel_id, channel_password = nil)
      all_or_array_client_ids(client_id_or_all).each do |clid|
        opts = { clid: clid, cid: channel_id }.merge(channel_password ? { cpw: channel_password } : {})
        Teamspeak::ActionSpeak.new.command_and_disconnect('clientmove', opts)
      end
      self
    end

    # def move_to(channel)
    #   # TODO make interface
    # end

    def kick(client_id_or_all, reason_id, message = nil)
      all_or_array_client_ids(client_id_or_all).each do |clid|
        opts = { clid: clid, reasonid: reason_id }.merge(message ? { reasonmsg: message } : {})
        Teamspeak::ActionSpeak.new.command_and_disconnect('clientkick', opts)
      end
      self.clid = nil
      self
    end

    %i[channel server].each do |action|
      define_method(:"#{action}_kick") do |client_id_or_all, message|
        kick(client_id_or_all, const_get(:"#{action.upcase}_CHANNEL_REASON_ID"), message)
      end
    end

    def poke(client_id_or_all = :all, message)
      all_or_array_client_ids(client_id_or_all).each do |clid|
        Teamspeak::ActionSpeak.new.command_and_disconnect('clientpoke', clid: clid, msg: message)
      end
      self
    end

    def send_message(client_id_or_all = :all, message)
      all_or_array_client_ids(client_id_or_all).each do |clid|
        Teamspeak::SendMessage.to_client(clid, message)
      end
      self
    end

    def info
      raise Teamspeak::DisconnectedClient unless online?
      Teamspeak::ActionSpeak.new.command_and_disconnect('clientinfo', clid: clid)
    end

    def edit(hash)
      hash.transform_keys!(&:to_sym).select!{ |key, _| %i[client_nickname client_is_talker client_description client_is_channel_commander client_icon_id] }

      Teamspeak::ActionSpeak.new.command_and_disconnect('clientedit', {clid: clid}.merge(hash))
    end


    private
    def all_or_array_client_ids(client_id_or_all)
      return client_ids if client_id_or_all == :all 
      [client_id_or_all].flatten
    end

    ##################################
    # EXCEPTIONS 
    ##################################
    
    class NotConnected < StandardError; end
  end
end
