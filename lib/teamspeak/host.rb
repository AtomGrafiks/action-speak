# frozen_string_literal: true

module Teamspeak
  # Displays detailed connection information about the server instance
  # including uptime, number of virtual servers online, traffic
  # information, etc.
  #
  # For detailed information, see {file:docs/ServerInstanceProperties.md Server Instance Properties}.
  class Host < ActionSpeak
    include Teamspeak::Exceptions::DynAccessor

    needed_permissions %i[b_serverinstance_info_view]

    def initialize
      super && command('hostinfo').each do |key, value|
        dyn_accessor(key, value)
      end
    end
  end
end
