# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class Ban < ActionSpeak
     include Teamspeak::Exceptions::DynAccessor

    attr_reader   :id

    @@cache_bans = nil

    needed_permissions %i[
      b_client_ban_list b_client_ban_create b_client_ban_delete b_client_ban_delete_own
    ]

    def initialize
      @connect_bypass = true
      super
    end

    def build(ban_id)
      @@cache_bans ||= Teamspeak::ActionSpeak.new.command_and_disconnect('banlist')
      return unless res = @@cache_bans.select { |h| h['banid'] == ban_id }.first
      res.each do |key, value|
        dyn_accessor(key.gsub(/^ban/, ''), value)
      end

      self
    end

    def prepare_hash(hash = {})
      hash.reject! do |k, _|
        attr_readers.include?(k.to_sym)
      end

      hash.transform_keys! { |k| "ban#{k}" }
    end

    def self.create(hash)
      check = %i[ip name uid]
      raise ArgumentError, "hash don't include ip or name or uid" unless (check & hash.transform_keys!(&:to_sym).keys).count == 0

      res = Teamspeak::ActionSpeak.new.command_and_disconnect("banadd", { time: time, banreason: hash[:reason] }.merge(hash))
      find(res['banid'])
    end

    def self.client!(clid, time, reason = '')
      Teamspeak::ActionSpeak.new.command_and_disconnect("banclient", {clid: clid, time: time, banreason: reason})
    end

    [[:delete, :del], [:delete_all, :delall]].each do |action|
      define_method(action.first) do
       Teamspeak::ActionSpeak.new.command_and_disconnect("ban#{action.last}", banid: id)
      end
    end

    def self.list
      Teamspeak::ActionSpeak.new.command_and_disconnect('banlist').map do |e|
        find(e['banid'])
      end
    end

    def self.find(banid)
      new.build(banid)
    end

    def self.find_by_uid(uid)
      Teamspeak::ActionSpeak.new.command_and_disconnect('banlist').select { |ban| ban['uid'] == uid }
    end
  end
end
