# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  class Server < ActionSpeak
    include Teamspeak::Exceptions::DynAccessor
    attr_reader   :id

    needed_permissions %i[
      b_virtualserver_info_view b_serverinstance_virtualserver_list b_virtualserver_delete
      b_virtualserver_create b_virtualserver_start_any b_virtualserver_start
      b_virtualserver_stop_any b_virtualserver_stop b_serverinstance_stop
      b_virtualserver_connectioninfo_view
      b_virtualserver_modify_name b_virtualserver_modify_welcomemessage
      b_virtualserver_modify_maxclients b_virtualserver_modify_reserved_slots
      b_virtualserver_modify_password b_virtualserver_modify_default_servergroup
      b_virtualserver_modify_default_channelgroup b_virtualserver_modify_default_channeladmingroup
      b_virtualserver_modify_ft_settings b_virtualserver_modify_ft_quotas
      b_virtualserver_modify_channel_forced_silence b_virtualserver_modify_complain
      b_virtualserver_modify_antiflood b_virtualserver_modify_hostmessage
      b_virtualserver_modify_hostbanner b_virtualserver_modify_hostbutton b_virtualserver_modify_port
      b_virtualserver_modify_autostart b_virtualserver_modify_needed_identity_security_level
      b_virtualserver_modify_priority_speaker_dimm_modificator b_virtualserver_modify_log_settings
      b_virtualserver_modify_icon_id b_virtualserver_modify_weblist b_virtualserver_modify_min_client_version
      b_virtualserver_modify_codec_encryption_mode
    ]

    def initialize
      super && build
    end

    def build
      command('serverinfo').each do |key, value|
        dyn_accessor(key.gsub(/^virtualserver_/, ''), value)
      end

      self
    end

    def prepare_hash(hash = {})
      hash.reject! do |k, _|
        attr_readers.include?(k.to_sym)
      end

      hash.transform_keys! { |k| "virtualserver_#{k}" }
    end

    def self.create(name, port, hash = {})
      as = new
      as.command_and_disconnect('servercreate', { virtualserver_name: name, virtualserver_port: port }.merge(as.prepare_hash(hash)))
      find_by_port(port)
    end

    def save
      command('serveredit', respond_to?(:prepare_hash) ? prepare_hash(super) : super)
    end

    %i[start stop delete].each do |action|
      define_method(action) do
        command("server#{action}", sid: id)
      end
    end

    def self.process_stop
      new.command_and_disconnect('serverprocessstop')
    end

    def self.list
      new.command('serverlist').map do |e|
        find(e['virtualserver_id'])
      end
    end

    def self.find(server_id)
      new.use(server_id) && new
    end

    def self.find_by_port(server_port)
      server_id = new.command_and_disconnect('serveridgetbyport', virtualserver_port: server_port)['server_id']
      find(server_id)
    end
  end
end
