# frozen_string_literal: true

module Teamspeak
  # Displays the servers instance information.
  # tokenid1={groupID} tokenid2={channelID}
  class PrivilegeKey < ActionSpeak
    include Teamspeak::Exceptions::DynAccessor
    attr_reader   :id

    @@cache_list

    def initialize
      @connect_bypass = true
      super
    end

    def build(token)
      @@cache_list ||= Teamspeak::ActionSpeak.new.command('privilegekeylist')
      @@cache_list.select { |h| h['token'] == token }.first.each do |key, value|
        dyn_accessor(key, value)
      end

      self
    end

    %w[use delete].each do |action|
      define_method(action) do
        Teamspeak::ActionSpeak.new.command("privilegekey#{action}", token: token)
      end
    end
    alias :destroy :delete

    # TODO :  Accept customfield
    #         format: tokencustomset=ident=forum_user value=Sven Paulsen|ident=forum_id value=123
    def self.create(type, group_id, channel_id, description = '')
      Teamspeak::ActionSpeak.new.command('privilegekeyadd', tokentype: type, token1: group_id, token2: channel_id, tokendescription: description)
    end

    def self.list
      Teamspeak::ActionSpeak.new.command('privilegekeylist').each do |key|
        new.build(key['token'])
      end
    end
  end
end
