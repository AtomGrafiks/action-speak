# frozen_string_literal: true

require 'ext/string'

require 'teamspeak/parser'
require 'teamspeak/exceptions'
require 'teamspeak/exceptions/dyn_accessor.rb'

require 'teamspeak/action_speak'

require 'teamspeak/version'
require 'teamspeak/host'
require 'teamspeak/instance'
require 'teamspeak/server'
require 'teamspeak/server_group'
require 'teamspeak/permission'
require 'teamspeak/snapshot'
require 'teamspeak/log'
require 'teamspeak/send_message'
require 'teamspeak/ban'
require 'teamspeak/complain'
require 'teamspeak/channel'
require 'teamspeak/channel_group'
require 'teamspeak/client'
require 'teamspeak/privilege_key'

module Teamspeak
  VERSION = '0.1.0a'
end
