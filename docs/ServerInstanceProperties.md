## Server Instance Properties

This is a list of properties available for the server instance:

| Name         | Information | Changeable? |
|--------------|-------------|-------------|
| INSTANCE_UPTIME | Uptime in seconds | No |
| HOST_TIMESTAMP_UTC | Current server date and time as UTC timestamp | No |
| VIRTUALSERVERS_RUNNING_TOTAL | Number of virtual servers running | No |
| CONNECTION_FILETRANSFER_BANDWIDTH_SENT | Current bandwidth used for outgoing file transfers (Bytes/s) | No |
| CONNECTION_FILETRANSFER_BANDWIDTH_RECEIVED | Current bandwidth used for incoming file transfers (Bytes/s) | No |
| CONNECTION_PACKETS_SENT_TOTAL | Total amount of packets sent | No |
| CONNECTION_PACKETS_RECEIVED_TOTAL | Total amount of packets received | No |
| CONNECTION_BYTES_SENT_TOTAL | Total amount of bytes sent | No |
| CONNECTION_BYTES_RECEIVED_TOTAL | Total amount of bytes received | No |
| CONNECTION_BANDWIDTH_SENT_LAST_SECOND_TOTAL | Average bandwidth used for outgoing data in the last second (Bytes/s) | No | 
| CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_TOTAL | Average bandwidth used for incoming data in the last second (Bytes/s) | No | 
| CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_TOTAL | Average bandwidth used for outgoing data in the last minute (Bytes/s) | No |
| CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_TOTAL | Average bandwidth used for incoming data in the last minute (Bytes/s) | No |
| SERVERINSTANCE_DATABASE_VERSION | Database revision number | No |
| SERVERINSTANCE_ GUEST_SERVERQUERY_GROUP | Default ServerQuery group ID | Yes |
| SERVERINSTANCE_TEMPLATE_SERVERADMIN_GROUP | Default template group ID for administrators on new virtual servers (used to create initial token) | Yes |
| SERVERINSTANCE_FILETRANSFER_PORT | TCP used for file tranfer | Yes |
| SERVERINSTANCE_MAX_DOWNLOAD_TOTAL_BANDWITDH | Max bandwidth available for outgoing file transfers (Bytes/s) | Yes |
| SERVERINSTANCE_MAX_UPLOAD_TOTAL_BANDWITDH | Max bandwidth available for incoming file transfers (Bytes/s) | Yes |
| SERVERINSTANCE_TEMPLATE_SERVERDEFAULT_GROUP | Default server group ID used in templates | Yes |
| SERVERINSTANCE_TEMPLATE_CHANNELDEFAULT_GROUP | Default channel group ID used in templates | Yes |
| SERVERINSTANCE_TEMPLATE_CHANNELADMIN_GROUP | Default channel administrator group ID used in templates | Yes |
| VIRTUALSERVERS_TOTAL_MAXCLIENTS | Max number of clients for all virtual servers | Yes |
| VIRTUALSERVERS_TOTAL_CLIENTS_ONLINE | Number of clients online on all virtual servers | Yes |
| VIRTUALSERVERS_TOTAL_CHANNELS_ONLINE | Number of channels on all virtual servers | Yes |
| SERVERINSTANCE_SERVERQUERY_FLOOD_COMMANDS | Max number of commands allowed in <SERVERINSTANCE_SERVERQUERY_FLOOD_TIME> seconds | Yes |
| SERVERINSTANCE_SERVERQUERY_FLOOD_TIME | Timeframe in seconds for <SERVERINSTANCE_SERVERQUERY_FLOOD_COMMANDS> commands | Yes |
| SERVERINSTANCE_SERVERQUERY_FLOOD_BAN_TIME | Time in seconds used for automatic bans triggered by the ServerQuery flood protection | Yes |